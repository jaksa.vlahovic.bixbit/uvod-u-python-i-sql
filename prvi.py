developers = {"Marshal": ["Company website", "Animal recognition", "Fashion store website"],
              "Ted": ["Plants watering system", "WHAT", "Animal recognition", "Food recognition"],
              "Monica": ["NEXT website", "Fashion store website"],
              "Phoebe": ["WHAT", "Company website", "NEXT website"]}


def main():
    projects = {}
    keys = []

    for key, val in developers.items():
        for item in val:
            if item not in keys:
                keys.append(item)
                projects[item] = [key]
            else:
                projects[item].append(key)

    print("Projects: ", projects)

    projects_list = list(projects.keys())
    projects_set = set(projects_list)

    projects_list = ["BIXBIT_" + project for project in projects_list]

    print(projects_list)

    # print("Novi broj developera: ", add_developer('Jaksa', ['Uvod u Python i SQL']))
    # print("Developeri: ", developers)

    add_info('Company website', 20, 200)
    add_info("Animal recognition", 30, 400)
    add_info("Fashion store website", 30, 400)
    add_info("Plants watering system", 30, 400)
    add_info("NEXT website", 30, 400)
    add_info("WHAT", 30, 400)
    add_info("Food recognition", 30, 400)

    print(developers)

    developers_info(projects, developers)


def add_developer(name, projects):
    developers[name] = projects
    return len(developers)


def add_info(project, time_spent, earnings):
    for key, val in developers.items():
        for index, item in enumerate(val):
            if item == project:
                val[index] = (item, str(time_spent) + "h", earnings)


def developers_info(projects, developers):
    for developer, project_list in developers.items():
        money_earned = 0
        time_spent = 0
        for project in project_list:
            name, time, earnings = project
            money_earned += earnings / len(projects[name])
            time_spent += int(time[:-1]) / len(projects[name])
        print("Developer ", developer, " zaradjuje $", round(money_earned / time_spent, 2), " po satu")


if __name__ == "__main__":
    main()
