from abc import ABC, abstractmethod
import re


class Film(ABC):

    def __init__(self, ime):
        self.ime = ime

    @abstractmethod
    def ocijeni_film(self):
        pass


class SciFi(Film):
    def __init__(self, ime, specijalni_efekti, vratolomije):
        super().__init__(ime)
        self.__specijalni_efekti = specijalni_efekti
        self.__vratolomije = vratolomije

    def set_specijalni_efekti(self, specijalni_efekti):
        if 1 < specijalni_efekti < 10:
            self.__specijalni_efekti = specijalni_efekti
        else:
            raise ValueError

    def set_vratolomije(self, vratolomije):
        if 1 < vratolomije < 10:
            self.__vratolomije = vratolomije
        else:
            raise ValueError

    def get_specijalni_efekti(self):
        return self.__specijalni_efekti

    def get_vratolomije(self):
        return self.__vratolomije

    def ocijeni_film(self):
        return (self.__specijalni_efekti + self.__vratolomije) / 2


class Dokumentarac(Film):
    def __init__(self, ime, vjerodostojnost, gledanost):
        super().__init__(ime)
        self.__vjerodostojnost = vjerodostojnost
        self.__gledanost = gledanost

    def set_vjerodostojnost(self, vjerodostojnost):
        if 0 < vjerodostojnost < 100:
            self.__vjerodostojnost = vjerodostojnost
        else:
            raise ValueError

    def set_gledanost(self, gledanost):
        if 0 < gledanost < 10:
            self.__gledanost = gledanost
        else:
            raise ValueError

    def get_vjerodostojnost(self):
        return self.__vjerodostojnost

    def get_gledanost(self):
        return self.__gledanost

    def ocijeni_film(self):
        return self.__vjerodostojnost / 100 * self.__gledanost


def main():
    film1 = SciFi("Lord of the Rings", 10, 10)
    print(film1.ime)
    print(film1.get_vratolomije())
    print(film1.get_specijalni_efekti())
    print(film1.ocijeni_film())

    film2 = Dokumentarac("Drive", 80, 9)
    print(film2.ime)
    print(film2.get_gledanost())
    print(film2.get_vjerodostojnost())
    print(film2.ocijeni_film())

    film_list = [film1, film2]
    new_list = []

    for film in film_list:
        if re.search("the", film.ime) and not re.search("and", film.ime):
            new_list.append(film)


if __name__ == "__main__":
    main()
