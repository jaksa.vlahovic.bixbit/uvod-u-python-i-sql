import pymysql.cursors

DB_NAME = "uvod_u_python_i_sql"


def main():
    db = pymysql.connect(host='localhost', user='jaksa', password='Jaksa01!')

    cursor = db.cursor()

    cursor.execute("""
        CREATE DATABASE IF NOT EXISTS uvod_u_python_i_sql
    """)

    db.select_db(DB_NAME)

    create_tables(cursor)

    # ubaci kategorije
    insert_or_delete_operation(db, cursor, """
        INSERT INTO firma_odjeljenja (naziv, lokacija, rukovodilac) VALUES ('IT', 'Niksic', 'Marko Markovic');
    """)
    insert_or_delete_operation(db, cursor, """
        INSERT INTO firma_odjeljenja (naziv, rukovodilac) VALUES ('HR', 'Marko Markovic');
    """)

    # ubaci zaposlene
    insert_or_delete_operation(db, cursor, """
        INSERT INTO firma_zaposleni (id, od, ime, telefon, datum_zaposlenja)
        VALUES (1, 2, 'Marko Markovic', '069111111', '2001-05-01');
    """)

    read_operation(cursor, """
        SELECT ime FROM firma_zaposleni;
    """)

    read_operation(cursor, """
        SELECT *
        FROM  firma_zaposleni, firma_odjeljenja
        WHERE firma_zaposleni.od = firma_odjeljenja.idod AND firma_odjeljenja.lokacija = 'Niksic';
    """)

    read_operation(cursor, """
        SELECT firma_zaposleni.ime, MIN(firma_zaposleni.datum_zaposlenja) AS datum_zaposlenja
        FROM firma_zaposleni
        INNER JOIN firma_odjeljenja
        ON firma_zaposleni.ime = 'Marko Markovic' AND firma_odjeljenja.rukovodilac = 'Marko Markovic';
    """)

    insert_or_delete_operation(db, cursor, """
        DELETE FROM firma_zaposleni
        WHERE firma_zaposleni.od IN (
                SELECT id
                FROM firma_odjeljenja
                WHERE naziv = 'marketing'
        );
    """)

    # drop_database(cursor, DB_NAME)

    db.close()


def insert_or_delete_operation(db, cursor, sql):
    try:
        cursor.execute(sql)
        db.commit()
    except:
        print("Operacija nije uspjela.")
        db.rollback()


def read_operation(cursor, sql):
    try:
        cursor.execute(sql)
        results = cursor.fetchall()
        print(results)
    except:
        print("Nije moguce procitati podatke.")


def create_tables(cursor):
    # napraviti tabelu odjeljenja firme ako ne postoji
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS firma_odjeljenja (
            idod INT AUTO_INCREMENT PRIMARY KEY,
            naziv VARCHAR(50),
            lokacija VARCHAR(50) DEFAULT 'Podgorica',
            rukovodilac VARCHAR(50),
            CONSTRAINT check_lokacija CHECK ( lokacija in ('Niksic', 'Podgorica', 'Zabljak', 'Bar') )
        );
        """
    )

    # napraviti tabelu zaposlenih firme ako ne postoji
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS firma_zaposleni (
            id INT PRIMARY KEY CHECK ( id BETWEEN 0 AND 999),
            od INT NOT NULL,
            ime VARCHAR(40),
            telefon VARCHAR(12),
            datum_zaposlenja DATE,
            CONSTRAINT fk_od FOREIGN KEY (od) REFERENCES firma_odjeljenja(idod)
        );
        """
    )


def drop_database(cursor, dbname):
    cursor.execute("DROP DATABASE IF EXISTS " + dbname)


if __name__ == '__main__':
    main()
