import pandas as pd


def read_from_csv():
    columns = ["positive", "hospitalized"]
    try:
        df = pd.read_csv("data_file.csv", usecols=columns)
        return df
    except FileNotFoundError:
        print("Fajl ne postoji.")
        exit(1)
