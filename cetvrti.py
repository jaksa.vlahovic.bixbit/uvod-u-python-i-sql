import datetime


class Osoba:
    def __init__(self, ime, prezime, jmbg, opstina, datum_rodjenja):
        self.ime = ime
        self.prezime = prezime
        self.opstina = opstina
        self.datum_rodjenja = datum_rodjenja
        self._jmbg = None

        self.set_jmbg(jmbg)
        self.set_datum_rodjenja(datum_rodjenja)

    def get_ime(self):
        return self.ime

    def get_prezime(self):
        return self.prezime

    def get_jmbg(self):
        return self._jmbg

    def get_opstina(self):
        return self.opstina

    def get_datum_rodjenja(self):
        return self.datum_rodjenja

    def set_ime(self, ime):
        self.ime = ime

    def set_prezime(self, prezime):
        self.prezime = prezime

    def set_jmbg(self, jmbg):
        if len(jmbg) == 13:
            self._jmbg = jmbg
        else:
            print("JMBG treba da ima 13 cifri, pokusaj ponovo")
            exit(1)

    def set_opstina(self, opstina):
        self.opstina = opstina

    def set_datum_rodjenja(self, datum_rodjenja):
        try:
            date = datetime.datetime.strptime(datum_rodjenja, '%Y-%m-%d')
            now = datetime.datetime.now()

            if date < now:
                self.datum_rodjenja = date
            else:
                raise ValueError

        except ValueError:
            print('Nije ispravan format, treba da bude YYYY-MM-DD i datum treba da bude raniji od danasnjeg')

    def __str__(self):
        return f"{self.ime} {self.prezime}"

    def __lt__(self, other):
        return self.datum_rodjenja > other.datum_rodjenja


def main():
    a = Osoba('Jaksa', 'Vlahovic', '0802001211055', 'Podgorica', '2001-02-08')
    b = Osoba('Matija', 'Vlahovic', '0306003211055', 'Podgorica', '2003-06-03')
    c = Osoba('John', 'Doe', '0306003211055', 'Podgorica', '1999-03-30')
    print("Najstariji: ", najstariji([a, b, c]))

    initial = 'y'

    while initial == 'y':
        print("Unesite osobu: (ime, prezime, jmbg, opstina, datum rodjenja) ")
        data = input()
        data = data.split(", ")
        try:
            [ime, prezime, jmbg, opstina, datum_rodjenja] = data
            nova_osoba = Osoba(ime, prezime, jmbg, opstina, datum_rodjenja)
            print("Nova osoba: ", nova_osoba)
            print("Zelite li da unesete jos osoba (y/n)?")
            initial = input()
        except ValueError:
            print("Pogresni argumenti uneseni, zelite li da probate opet (y/n)?")
            initial = input()


def najstariji(osobe):
    najstarija_osoba = osobe[0]
    for osoba in osobe:
        if osoba > najstarija_osoba:
            najstarija_osoba = osoba

    return najstarija_osoba


if __name__ == "__main__":
    main()
