import json
import csv


def hospitalization_percentage(date):
    with open('covid-data.json') as file:
        file_data = json.load(file)

    for item in file_data:
        if item['date'] == date:
            return round(item['hospitalized'] / item['positive'], 2)


def main():
    with open('covid-data.json') as json_file:
        data = json.load(json_file)

    positive_cases = []
    hospitalized_cases = []

    data_file = open('data_file.csv', 'w')
    csv_writer = csv.writer(data_file)
    csv_writer.writerow(['positive', 'hospitalized'])

    for day in data:
        if 20210300 > day['date'] > 20210131:
            positive_cases.append(day['positiveIncrease'])
            hospitalized_cases.append(day['hospitalizedIncrease'])
            csv_writer.writerow([day['positiveIncrease'], day['hospitalizedIncrease']])
            print(hospitalization_percentage(day['date']))

    print("Upisano u fajl data_file.csv")


if __name__ == "__main__":
    main()
