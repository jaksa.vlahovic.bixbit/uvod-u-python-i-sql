from cetvrti import Osoba


class Radnik(Osoba):
    def __init__(self, ime, prezime, jmbg, opstina, datum_rodjenja, zanimanje, plata, godine_staza):
        super().__init__(ime, prezime, jmbg, opstina, datum_rodjenja)
        self.zanimanje = zanimanje
        self.__plata = plata
        self.godine_staza = godine_staza

    def get_plata(self):
        return self.__plata

    def godisnji_prihod(self):
        return self.get_plata() * 12

    def godina_rodjenja(self):
        return self.get_datum_rodjenja().year

    def radnik_tuple(self):
        return self.ime, self.prezime, self.godina_rodjenja(), self.godine_staza, self.godisnji_prihod()


def sort_radnike(radnici):
    def sort_fn(radnik):
        return radnik.godisnji_prihod()

    return sorted(radnici, key=sort_fn, reverse=True)


def uporedi_sortiranje(radnici):
    def sort_godiste(radnik):
        return radnik.godina_rodjenja()

    def sort_staz(radnik):
        return radnik.godine_staza

    radnici_plata = sort_radnike(radnici)
    radnici_godiste = sorted(radnici, key=sort_godiste)
    radnici_staz = sorted(radnici, key=sort_staz, reverse=True)

    if radnici_plata == radnici_godiste == radnici_staz:
        print("Radnici se isto sortiraju po plati, godistu i stazu.")
    else:
        print("Radnici se ne sortiraju isto po plati, godistu i stazu.")


def main():
    radnik1 = Radnik('Jaksa', 'Vlahovic', '0802001211055', 'Podgorica', '2001-02-08', 'developer', 20, 2)
    radnik2 = Radnik('Matija', 'Vlahovic', '0802001211055', 'Podgorica', '2003-02-08', 'developer', 50, 0)
    print(radnik1.radnik_tuple())
    radnici = [radnik1, radnik2]
    radnici_sorted = sort_radnike(radnici)
    uporedi_sortiranje(radnici)


if __name__ == '__main__':
    main()
