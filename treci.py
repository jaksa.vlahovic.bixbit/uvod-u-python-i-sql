from matplotlib import pyplot as plt
from treci_func import read_from_csv


def main():
    plt.rcParams["figure.autolayout"] = True

    df = read_from_csv()

    plt.plot(df['positive'], label="positive")
    plt.plot(df['hospitalized'], label="hospitalized")
    plt.show()


if __name__ == "__main__":
    main()
