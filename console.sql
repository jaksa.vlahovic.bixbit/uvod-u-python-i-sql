CREATE DATABASE uvod_u_python_i_sql;

# a)
CREATE TABLE IF NOT EXISTS firma_odjeljenja
(
    idod        INT AUTO_INCREMENT PRIMARY KEY,
    naziv       VARCHAR(50),
    lokacija    VARCHAR(50) DEFAULT 'Podgorica',
    rukovodilac VARCHAR(50),
    CONSTRAINT check_lokacija CHECK ( lokacija in ('Niksic', 'Podgorica', 'Zabljak', 'Bar') )
);

CREATE TABLE IF NOT EXISTS firma_zaposleni
(
    id               INT PRIMARY KEY CHECK ( id BETWEEN 0 AND 999),
    od               INT NOT NULL,
    ime              VARCHAR(40),
    telefon          VARCHAR(12),
    datum_zaposlenja DATE,
    CONSTRAINT fk_od FOREIGN KEY (od) REFERENCES firma_odjeljenja (idod)
);

INSERT INTO firma_odjeljenja (naziv, lokacija, rukovodilac)
VALUES ('IT', 'Niksic', 'John Doe');

INSERT INTO firma_odjeljenja (naziv, rukovodilac)
VALUES ('HR', 'John Doe');

INSERT INTO firma_odjeljenja (naziv, rukovodilac)
VALUES ('marketing', 'John Doe');

INSERT INTO firma_zaposleni (id, od, ime, telefon, datum_zaposlenja)
VALUES (1, 2, 'Matija Vlahovic', '069111111', '2022-05-01');

INSERT INTO firma_zaposleni (id, od, ime, telefon, datum_zaposlenja)
VALUES (2, 1, 'Jaksa Vlahovic', '069111111', '2022-05-01');

# b)
SELECT ime
FROM firma_zaposleni;

# c)
# sa select iz vise tabela
SELECT *
FROM firma_zaposleni,
     firma_odjeljenja
WHERE firma_zaposleni.od = firma_odjeljenja.idod
  AND firma_odjeljenja.lokacija = 'Niksic';

# sa join
SELECT *
FROM firma_zaposleni
         INNER JOIN firma_odjeljenja
                    ON firma_zaposleni.od = firma_odjeljenja.idod
WHERE firma_odjeljenja.lokacija = 'Niksic';

# d)
SELECT firma_zaposleni.ime, MIN(firma_zaposleni.datum_zaposlenja) AS datum_zaposlenja
FROM firma_zaposleni
         INNER JOIN firma_odjeljenja
                    ON firma_zaposleni.ime = 'Marko Markovic' AND firma_odjeljenja.rukovodilac = 'Marko Markovic';

# e)
DELETE
FROM firma_zaposleni
WHERE firma_zaposleni.od IN (SELECT id
                             FROM firma_odjeljenja
                             WHERE naziv = 'marketing');

DROP TABLE firma_zaposleni;
DROP TABLE firma_odjeljenja;
